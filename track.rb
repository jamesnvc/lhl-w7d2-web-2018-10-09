class Track < ActiveRecord::Base
  belongs_to :record, inverse_of: :tracks
  has_and_belongs_to_many :playlists, join_table: :playlist_tracks
end
