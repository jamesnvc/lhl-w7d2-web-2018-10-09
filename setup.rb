require 'active_record'
require 'pg'
require 'pry'
require 'faker'

require_relative './artist'
require_relative './record'
require_relative './track'
require_relative './playlist'

# Set up the database connection
ActiveRecord::Base.establish_connection(
  adapter: "postgresql",
  hostname: "localhost",
  username: "tester", # this will vary depending on your install of postgres
  password: "tester", # likewise
  # You may need to explicitly run a command to create this database
  # e.g. with Postgres, it would be `createdb lhlw7d2 -O tester` for
  # this config
  database: "lhlw7d2"
)

# Define all the database tables
ActiveRecord::Schema.define do

  create_table :artists, force: true do |table|
    table.string :name
    table.timestamps
  end

  create_table :records, force: true do |t|
    t.string :title
    t.references :artist
  end

  create_table :tracks, force: true do |t|
    t.string :title
    t.integer :duration
    t.references :record
  end

  create_table :playlists, force: true do |t|
    t.string :name
  end

  create_join_table :playlists, :tracks, force: true, table_name: :playlist_tracks do |t|
  end
end

# Seed some data
10.times do
  a = Artist.create!(name: Faker::Music.band)

  rand(3..5).times do
    r = Record.create!(title: Faker::Music.album, artist: a)
    rand(7..10).times do
      Track.create!(title: Faker::Lebowski.quote,
                    duration: rand(120..360),
                    record: r)
    end
  end
end

tracks = Track.all
5.times do

  p = Playlist.create!(name: Faker::Kpop.girl_groups)
  rand(2..5).times do
    p.tracks << tracks.sample
  end
end

# Setting up a logger so we can see the SQL queries that the ORM generates
ActiveRecord::Base.logger = ActiveSupport::Logger.new(STDOUT)

# Drop into an interactive shell at this point
binding.pry
