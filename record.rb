class Record < ActiveRecord::Base
  belongs_to :artist, inverse_of: :records
  has_many :tracks
end
