class Playlist < ActiveRecord::Base
  has_and_belongs_to_many :tracks, join_table: :playlist_tracks
end
